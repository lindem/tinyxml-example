CXX=clang++
CXXFLAGS=-Wall -c -ggdb
LDFLAGS=-ltinyxml2

parse: main.o
	${CXX} ${LDFLAGS} $< -o $@

.cc.o:
	${CXX} ${CXXFLAGS} $< -o $@

clean:
	rm parse main.o
