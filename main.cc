#include <tinyxml2.h>
#include <iostream>

using namespace std;
using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;

int main(int argc, char* argv[]) {
  XMLDocument doc;
  doc.LoadFile("./doc.xml");
  for (XMLElement* person = doc.FirstChildElement("people")->FirstChildElement("person");
       person != 0;
       person = person->NextSiblingElement("person")) {
	 const char* vorname = person->FirstChildElement("firstname")->GetText();
	 const char* nachname = person->FirstChildElement("lastname")->GetText();
	 cout << "Vorname: " << vorname << ", Nachname: " << nachname << endl;
       }
}
